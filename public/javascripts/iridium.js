document.getElementById('registrationLink').onclick = (elem) => {
    $('#authentication').hide();
    $('#registration').show();
    document.getElementById('backToAuthorization').onclick = (elem) => {
        $('#registration').hide();
        $('#authentication').show();
        elem.preventDefault();
    };
    elem.preventDefault();
};

document.getElementById('registrationLinkAbout').onclick = (elem) => {
    $('#registration').show();
    $('#about').hide();
    elem.preventDefault();
    document.getElementById('backToAuthorization').onclick = (elem) => {
        $('#registration').hide();
        $('#about').show();
        elem.preventDefault();
    };
};

document.getElementById('aboutLink').onclick = (elem) => {
    $('.infoContainer').hide();
    $('#about').show();
    elem.preventDefault();
};

document.getElementById('messagingLink').onclick = (elem) => {
    $('.infoContainer').hide();
    $('#mainpage').show();
    elem.preventDefault();
};

let chatsApp;
let chatApp;

document.getElementById('navName').onclick = (elem) => elem.preventDefault();

const socket = io('http://localhost:3000');
socket.on('connect', () => console.log('connect'));


const loginInput = document.getElementById('registrationLogin');
loginInput.oninput = () => {
    const value = loginInput.value;
    document.getElementById('registrationSubmitButton').disabled = true;
    if (value)
        socket.emit('checkLogin', value, (data) => {
            if (data) {
                loginInput.setCustomValidity('');      
                $('#inValidLogin').hide();
                $('#validLogin').show();
                document.getElementById('registrationSubmitButton').disabled = false;
            } else {
                loginInput.setCustomValidity('notValid');
                $('#validLogin').hide();
                $('#inValidLogin').show();
                document.getElementById('registrationSubmitButton').disabled = true;
            }
        });
}

const tagInput = document.getElementById('tagInput');
tagInput.oninput = () => {
    const value = tagInput.value;
    document.getElementById('creatingChatButton').disabled = true;
    if (value)
        socket.emit('chatTagCheck', value, (data) => {
            if (data) {
                tagInput.setCustomValidity('');      
                $('#inValidTag').hide();
                $('#validTag').show();
                document.getElementById('creatingChatButton').disabled = false;
            } else {
                tagInput.setCustomValidity('notValid');
                $('#validTag').hide();
                $('#inValidTag').show();
                document.getElementById('creatingChatButton').disabled = true;
            }
        });
}

const passwordInput = document.getElementById('registrationPassword');
passwordInput.oninput = () => {
    const value = passwordInput.value;
    const passwordValid = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");
    if (passwordValid.test(value)){
        passwordInput.setCustomValidity('');      
        $('#inValidPassword').hide();
        $('#validPassword').show();
    } else {
        passwordInput.setCustomValidity('notValid');
        $('#validPassword').hide();
        $('#inValidPassword').show();
    }
    const conPasValue = confirmPasswordInput.value;
    confirmPasswordInput.setCustomValidity(value === conPasValue ? '' : 'f');
}

const confirmPasswordInput = document.getElementById('registrationConfirmPassword');
confirmPasswordInput.oninput = () => {
    const pasValue = passwordInput.value;
    const conPasValue = confirmPasswordInput.value;
    confirmPasswordInput.setCustomValidity(pasValue === conPasValue ? '' : 'f');
}

document.getElementById('registrationForm').onsubmit = (elem) => {
    elem.preventDefault();
    const formData = new FormData(elem.target);
    const data = getFormDataAsJSON(formData);
    socket.emit('registration', data, token => {
        localStorage.setItem('jwt', token);
    });
}

document.getElementById('authenticationForm').onsubmit = (elem) => {
    elem.preventDefault();
    const formData = new FormData(elem.target);
    const data = getFormDataAsJSON(formData);
    socket.emit('authentication', data, (token) => {
        if (token)
            localStorage.setItem('jwt', token);
        else{
            $('#authorizationProblem').show();
            document.getElementById('authorizationProblem').innerText = 'Login or password not right';
        }    
    });
};

function authenticateByJWT() {
    const token = localStorage.getItem('jwt');
    socket.emit('JWTauthentication', token, (auth, mes) => {
        if(auth)
            localStorage.setItem('jwt', mes);
        else {
            $('#authorizationProblem').show();
            document.getElementById('authorizationProblem').innerHTML = mes;
        }
    });
}


function getFormDataAsJSON(formData) {
    let jsonObject = {};
    formData.forEach((value, key) => {
        jsonObject[key] = value;
    });
    return jsonObject;
}

if(localStorage.getItem('jwt')) 
    authenticateByJWT();

socket.on('authorised', (user) => {
    $('.infoContainer').hide();
    $('#mainpage').show();
    $('.forAuthorisedLink').show();
    if (user.role > 0)
        $('.forAdminLink').show();
    console.log(user.chats);
    user.chats.sort(function(a,b){
        return new Date(b.lastmessagedate) - new Date(a.lastmessagedate);
    });
    chatsApp = new Vue({
        el: '#chatsContainer',
        data: {
            chats: user.chats,
            userId: user._id
        },
        methods: {
            sortChats: () => {
                this.chats.sort(function(a,b){
                    return new Date(b.lastmessagedate) - new Date(a.lastmessagedate);
                });
            },
            updateChat: async function (message)  {
                const index = this.chats.findIndex(x => x._id == message.chat);
                let chat = this.chats.splice(index, 1);
                chat[0].lastmessage = message;
                chat[0].lastmessagedata = message.data;
                chat[0].lastmessagedate = message.date;
                this.chats.unshift(chat[0]);
                await this.$forceUpdate();
                subscribeToChoosingChat();
                console.log(chatApp.chat._id === this.chats[0]._id);
                console.log(chatApp.chat._id == this.chats[0]._id);
                if (chatApp && chatApp.chat._id === this.chats[0]._id){
                    $('.selected_chat').removeClass('selected_chat');
                    document.getElementById(chatApp.chat._id).classList.add('selected_chat');
                } else {
                    this.chats[0].newMessage=true;
                    this.$forceUpdate();
                } 

            }
        }
    });
    subscribeToChoosingChat();
});

function subscribeToChoosingChat() {
    const chatLinks = document.getElementsByClassName('chat_link');
    for (let i = 0; i < chatLinks.length; i++){
        const chatItem = chatLinks.item(i); 
        chatItem.onclick = () => {
            selectingChat(chatItem.id);
            $('.selected_chat').removeClass('selected_chat');
            chatItem.classList.add('selected_chat');
        };
    }
}

function selectingChat(id){
    socket.emit('getChat', id , async (status, chat, userId) => {
        if (status !== 200)
            console.log(chat);
        else if (!chatApp)
            createChatApp(chat, userId);
        else 
            chatApp.chat = chat;
        chatApp.fillchat();
        $('#chat').show();
        document.getElementById('sendButton').disabled = false;
        if (chat.users.find(x => x._id === userId))
            document.getElementById('sendButton').innerHTML = 'Send';    
        else
            document.getElementById('sendButton').innerHTML = 'Join';
        await chatApp.$forceUpdate();
        document.getElementById(chat.lastmessage).scrollIntoView();
    });
    const index = chatsApp.chats.findIndex(x => x._id == id);
    chatsApp.chats[index].newMessage = false;
    chatsApp.$forceUpdate();
};

function createChatApp(chat, userId) {
    chatApp = new Vue({
        el: '#chat',
        data: {
            chat: chat, 
            userId: userId
        },
        methods: {
            fillchat: function() {
                if (this.chat.messages) this.chat.messages.forEach(mes => {
                    const user = this.chat.users.filter(x => x._id === mes.sender);
                    if (user[0])
                        mes.senderName = user[0].fullname;
                    else{
                        mes.senderName = 'Deleted user';
                        mes.sender = '';
                    }
                    this.$forceUpdate();
                });
            },
            addMessage: async function (message) {
                const user = this.chat.users.filter(x => x._id === message.sender);
                    if (user[0])
                        message.senderName = user[0].fullname;
                    else
                        message.senderName = 'Deleted user'
                chatApp.chat.messages.push(message);
                await chatApp.$forceUpdate();
            },
            updateMessage: function (message) {
                const index = chatApp.chat.messages.findIndex(mes => mes._id == message._id);
                chatApp.chat.messages[index] = message;
                chatApp.$forceUpdate();
            }
        }
    });
}

document.getElementById('sendingForm').onsubmit = function (elem){
    elem.preventDefault();
    if (document.getElementById('sendButton').innerHTML === 'Send'){
        const formData = new FormData(elem.target);
        let data = getFormDataAsJSON(formData);
        data.sender = chatApp.userId;
        data.chat = chatApp.chat._id;
        document.getElementById('sendText').value = '';
        document.getElementById('fileinput').value = '';
        if (data.data || data.file)
            socket.emit('sendMessage', data);
    } else {
        socket.emit('joinChat', chatApp.chat._id, chatApp.userId, () => {
            selectingChat(chatApp.chat._id);
        });
    }
};

$("#sendText").keypress(function (e) {
    if(e.which == 13 && !e.shiftKey) {        
        $('#sendingForm').submit();
        e.preventDefault();
        return false;
    }
});

socket.on('newMessage', async (message) => {
    await chatsApp.updateChat(message);
    if (message.sender != chatsApp.userId)
        document.getElementById("audio").play();
    if (chatApp.chat._id == message.chat){
        await chatApp.addMessage(message);
        document.getElementById(message._id).scrollIntoView();
    }
});

document.getElementById('creatingChatForm').onsubmit = (elem) => {
    elem.preventDefault();
    $('#chatCreating').modal('hide');
    const formData = new FormData(elem.target);
    let data = getFormDataAsJSON(formData);
    data.users = [chatsApp.userId];
    data.usersRoling = [{user: chatsApp.userId, role: 2}];
    socket.emit('newChat', data, async chat => {
        chatsApp.chats.push(chat);
        if (!chatApp)
            createChatApp(chat, chatsApp.userId);
        else
            chatApp.chat = chat;
        chatApp.fillchat();
        $('#chat').show();
        document.getElementById('sendButton').disabled = false;
        await chatsApp.$forceUpdate(); 
        subscribeToChoosingChat(); 
        document.getElementById(chat._id).click();  
    });
};

document.getElementById("sendButton").disabled = true;

document.getElementById('searchBar').onsubmit = (elem) => {
    elem.preventDefault();
    $('#addChat').hide();
    $('#back').show();
    const query = document.getElementById('search').value;
    socket.emit('getChats', query, async (chats) => {
        console.log(chats);
        chatsApp.chats = chats;
        await chatsApp.$forceUpdate(); 
        subscribeToChoosingChat();
    });
};

document.getElementById("back").onclick = () => {
    socket.emit('getMyChats', chatsApp.userId, async (chats) => {
        chatsApp.chats = chats;
        await chatsApp.$forceUpdate(); 
        subscribeToChoosingChat();
    });
    $('#addChat').show();
    $('#back').hide();
}

$('#messageEditModal').on('show.bs.modal', function (event) {
    const message = $(event.relatedTarget);
    const id = message.attr('id');
    const data = message.data('whatever');
    $('#editMessageTextarea').val(data);
    $('#editMessageId').val(id);
});

let userApp;

$('#userModal').on('show.bs.modal', function (event) {
    const message = $(event.relatedTarget);
    const id = message.data('user');
    $('#user-container').hide();
    socket.emit('getUser', id, (user) => {
        if (!userApp)
            userApp = new Vue({
                el: '#user-container',
                data: {
                    user: user
                }
            }); 
        else 
            userApp.user = user;
        $('#user-container').show();
    });
});

let chatModal;

$('#chatModal').on('show.bs.modal', function (event) {
    if (!chatModal)
        chatModal = new Vue({
            el: '#chat-modal-container',
            data: {
                chat : chatApp.chat
            }
        }); 
    else 
        chatModal.chat = chatApp.chat;
        
});

$('#myProfileModal').on('show.bs.modal', function (event) {
    
    socket.emit('getUser', chatsApp.userId, (user) => {
        console.log(chatsApp.userId);
        console.log(user);
        
        document.getElementById('myProfileLogin').value = user.login;
        document.getElementById('myProfileFullname').value = user.fullname;
        document.getElementById('myProfileBio').value = user.bio;
    });
});

socket.on('updatedMessage', message => {
    if (chatApp && message.chat == chatApp.chat._id){
        chatApp.updateMessage(message);
    }
});

socket.on('removedMessage', mesId => {
    if (chatApp){
        const index = chatApp.chat.messages.findIndex(mes => mes._id == mesId);
        chatApp.chat.messages.splice(index, 1);
    }
});

document.getElementById('messageEditButton').onclick = (elem) => {
    const formData = new FormData(document.getElementById('editMessageForm'));
    const data = getFormDataAsJSON(formData);
    if (data.data)
        socket.emit('editMessage', data.messageId, data.data);
};

document.getElementById('removeMessageButton').onclick = (elem) => {
    const formData = new FormData(document.getElementById('editMessageForm'));
    const data = getFormDataAsJSON(formData);
    $('#confirmDeleteModal').modal('show');
    document.getElementById('confirmDeleting').onclick = (elem) => {
        socket.emit('removeMessage', data.messageId);
    };
};

document.getElementById('profileEditButton').onclick = (elem) => {
    const formData = new FormData(document.getElementById('editMyProfile'));
    const data = getFormDataAsJSON(formData);
    socket.emit('editProfile', chatsApp.userId, data);
};

document.getElementById('logout').onclick = (elem) => {
    localStorage.removeItem('jwt');
    window.location.href = '/';
};

let usersApp;

$('#usersModal').on('show.bs.modal', function (event) {
    
    socket.emit('getUsers', 1, (users, fullRights) => {
        console.log(users);
        console.log(fullRights);
        if (users)
            usersApp = new Vue({
                el: '#users',
                data: {
                    docs: users.docs,
                    page: users.page,
                    pages: users.pages,
                    fullrights: fullRights 
                },
                methods: {
                    previous:  function(){
                        if (this.page !== 1)
                            socket.emit('getUsers', this.page - 1, (users) => {
                                this.docs = users.docs;
                                this.page = users.page;
                                this.pages = users.pages;
                            });
                    },
                    next:  function(){
                        if (this.page !== this.pages)
                            socket.emit('getUsers', this.page + 1, (users) => {
                                this.docs = users.docs;
                                this.page = users.page;
                                this.pages = users.pages;
                            });
                    }, 
                    giveRights: function(event){
                        socket.emit('giveRights', event.target.name);
                    }, 
                    removeRights: function(event){  
                        socket.emit('removeRights', event.target.name);
                    }, 
                    removeUser: function(event){
                        socket.emit('removeUser', event.target.name, (cond) => {
                            if (cond)
                            socket.emit('getUsers', this.page, (users) => {
                                this.docs = users.docs;
                                this.page = users.page;
                                this.pages = users.pages;
                            }); 
                        });
                    }     
                }
            });
        else
            console.log(users);
        
    });
});