let app;

const connection = new WebSocket('wss://localhost:3000');
connection.addEventListener('open', () => console.log(`Connected to ws server`));
connection.addEventListener('error', (err) => console.error(`ws error: ${err}`));
connection.addEventListener('message', (message) => {
    console.log(`ws message: ${message.data}`);
    addNewMessage(message.data);
});


fetch("/api/chats")
    .then(x => x.json())
    .then((chatsData) => {
        app = new Vue({
            el: '#app',
            data: {
                chats: chatsData
            }
        });
    })
    .catch(err => console.error(err));