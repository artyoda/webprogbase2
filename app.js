const express = require('express');
const path = require('path');
const busBoyBodyParser = require("busboy-body-parser");
const http = require('http');

const app = express();
const server = http.createServer(app);

app.use(busBoyBodyParser());
app.use(express.static('public'));
const mongoose = require('mongoose');

const config = require('./config');
const dataBaseURL = config.dataBaseURL;
const port = config.serverPort;

const userRouter = require("./routers/user");
app.use("/", userRouter);

const chatRouter = require("./routers/chat");
app.use("/", chatRouter);

const messageRouter = require("./routers/message");
app.use("/", messageRouter);

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/index.html'));
});

app.get('/*', (req, res) => {
    res.status(404).sendFile(path.join(__dirname, 'views/404.html'));
});

const SocketServer = require('./modules/socketserver');
const socketServer = new SocketServer(server);

mongoose.connect(dataBaseURL, { useNewUrlParser: true })
        .then(() => server.listen(port, () => console.log(`Server is ready on ${port}`)))
        .catch(err => console.log(err));