const WebSocketServer = require('ws').Server;
const io = require('socket.io');
const User = require('../models/user');
const Chat = require('../models/chat');
const Message = require('../models/message');
const config = require('../config');
const jwt = require('jsonwebtoken');

const cloudinary = require('../models/cloudinary');

const crypto = require('crypto');
const serverSalt = config.salt;

function sha512(password){
    const hash = crypto.createHmac('sha512', serverSalt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: serverSalt,
        passwordHash: value
    };
}

class SocketServer {
    constructor(server) {
        this.ioServer = new io(server);
        this.ioServer.on('connection', socket => this.onConnect(socket));
    }

    onDisconnect(socket) {
        console.log('Disconnect!');
    }

    onConnect(socket) {
        socket.on('checkLogin', this.checkLogin);
        socket.on('registration', async (data, callback) => {
            const auth = await this.register(data, callback);
            if (auth) 
                this.onAuthorized(socket, auth);
        });
        socket.on('JWTauthentication', async (token, callback) =>  {
            const auth = await this.JWTauthentication(token, callback);
            if (auth) 
                this.onAuthorized(socket, auth);
        });
        socket.on('authentication', async (data, callback) => {
            const auth = await this.authentication(data, callback);
            if (auth) 
                this.onAuthorized(socket, auth);
        });
        socket.on('disconnect', this.onDisconnect);
        console.log('New connection!');
    }

    async onAuthorized(socket, user){
        //events for authorized users
        socket.on('getChat', (id, callback) => this.getChat(id, user, callback));
        socket.on('sendMessage', async (mes) => this.newMessage(mes));
        socket.on('chatTagCheck', this.checkTag);
        socket.on('newChat', async (data, callback) => {
            const chat = await this.newChat(data, callback);
            socket.join(chat._id);
        });

        socket.on('getChats', (query, callback) => {
            this.search(query, callback);
        });

        socket.on('getMyChats', async (id, callback) => {
            const user = await User.getById(id);
            callback(user.chats);
        });

        socket.on('getMyProfile', async (id, callback) => {
            const user = await User.getById(id);
            callback(user);
        });

        socket.on('editMessage', (mesId, data) => {
            this.updateMessage(mesId, data);
        });

        socket.on('editProfile', (id, data) => {
            this.updateProfile(id, data);
        });

        socket.on('removeMessage', (mesId) => {
            this.removeMessage(mesId);
        });

        socket.on('getUser', (userId, callback) => this.getUser(userId, callback));

        socket.on('joinChat', async (chatId, userId, callback) => {
            await User.joinChat(userId, chatId);
            await Chat.addUser(userId, chatId);
            socket.join(chatId);
            callback();
        });
        
        let fullinfo = await User.getById(user._id);
        
        socket.on('getUsers', async (page, callback) => {
            if (fullinfo.role > 0)
                callback(await User.getAll(page), fullinfo.role === 2 ? true : false);
            else
                callback(false);

        });

        socket.on('giveRights', (id) => {
            if (fullinfo.role === 2)
                User.giveRights(id);
        });

        socket.on('removeRights', (id) => {
            if (fullinfo.role === 2)
                User.removeRights(id);
        });

        socket.on('removeUser', async (id, callback) => {
            const user = await User.getById(id);
            if (fullinfo.role > user.role){
                await User.remove(id);
                callback(true);
            } else
                callback(false);
        });

        fullinfo.chats.forEach(chat => {
            socket.join(chat._id);
        });
        socket.emit('authorised', fullinfo);
    }

    async getUser(userId, callback) {
        try {
            const user = await User.getById(userId);
            callback({
                fullname: user.fullname,
                login: user.login,
                bio: user.bio
            });
        } catch (err) {
            callback();
        }
    }

    async updateProfile(id, data) {
        let user = await User.getById(id);
        user.bio = data.bio;
        user.fullname = data.fullname;
        await User.update(id, user);
    }

    async search(query, callback){
        if (query[0] === '@'){
            query = query.substr(1);
            const chat = await Chat.getByTag(query);
            if (chat) 
                callback([chat]);
            else
                callback();
        }
        else {
            const chats = await Chat.getAll(query);
            callback(chats.docs);
        } 
    } 

    async newChat(data, callback) {
        const chat = await Chat.insert(data);
        await User.joinChat(data.users[0], chat._id);
        callback(chat);
        return chat;
    }

    async newMessage(mes){
        
        cloudinary(mes.file, async (pinned) => {
            mes.pinned = pinned;
            const chat = mes.chat;
            const message = await Message.insert(mes);
            const prom = await Chat.addMessage(message, chat);
            this.ioServer.to(chat).emit('newMessage', message);
        });
    }

    async removeMessage(mesId) {
        const message = await Message.remove(mesId);
        this.ioServer.to(message.chat).emit('removedMessage', mesId);
    }

    async updateMessage(mesId, data) {
        let message = await Message.getById(mesId);
        message.data = data;
        let newmes = await Message.update(mesId, message);
        newmes.data = message.data;
        this.ioServer.to(newmes.chat).emit('updatedMessage', newmes);
    }

    async getChat(id, user, callback){
        try {
            const chat = await Chat.getById(id); 
            let cond = false;
            for (let i of chat.users)
                if (i.equals(user._id))
                    cond = true;
            if (cond || chat.isPublic)
                callback(200, chat, user._id);
            else
                callback(403, 'Forbidden', user._id);
            
        } catch (err) {
            callback(404, 'Not found', user._id);
        }
    }

    async authentication(data, callback){
        data.password = sha512(data.password).passwordHash;
        const user = await User.getByLoginAndHashPass(data.username, data.password);
        if (user){
            callback(jwt.sign(user.toJSON(), config.jwtSecret, {
                expiresIn: 2419200 // 4 weeks
            }));
            return user;
        } else {
            callback(false);
            return false;
        }
    }

    async register(data, callback) {
        data.password = sha512(data.password).passwordHash;
        const user = await User.insert(data);
        const token = jwt.sign(user.toJSON(), config.jwtSecret, {
            expiresIn: 2419200 // 4 weeks
        });
        callback(token);
        return user;
    }

    async JWTauthentication(token, callback){
        try {
            const decoded = jwt.verify(token, config.jwtSecret);
            try {
                const user = await User.verificationById(decoded._id);
                if (user.login === decoded.login && user.password === decoded.password){
                    callback(true, jwt.sign(user.toJSON(), config.jwtSecret, {
                        expiresIn: 2419200 // 4 weeks
                    }));
                    return user;
                }      
                else {
                    callback(false, 'Login or password was changed from last authentication!');
                    return false;
                } 
            } catch (err) {
                callback(false, 'There is no such user! The user must be deleted!');
                return false;
            }
        } catch (err) {
            callback(false, 'Authorized time exceeded!');
            return false;
        }
    }

    async checkLogin(login, callback) {
        const user = await User.getByLogin(login);
        callback(!user);
    }

    async checkTag(tag, callback) {
        const chat = await Chat.getByTag(tag);
        callback(!chat);
    }
}

module.exports = SocketServer;