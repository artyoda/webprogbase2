const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const mongoosePaginate = require('mongoose-paginate');
const User = require('./user');

const MessageSchema = new Schema({
    chat: {type: ObjectId, ref: 'Chat', required: true},
    senderName: String,
    sender: {type: ObjectId, ref: 'User', default: '5bde2ac37e3e354a5685f534'},
    data: String,
    time: {type: Date, default: Date.now()},
    isSeen: {type:Boolean, default: false},
    isEdited: {type: Boolean, default: false},
    pinned: String
});

MessageSchema.plugin(mongoosePaginate);

const MessageModel = mongoose.model('Message', MessageSchema);

class Message{

    static insert(mes){
        return  MessageModel.create(mes);
    }

    static getAll(){
        return  MessageModel.find({})
                            .populate('chat')
                            .populate('user')
                            .exec();
    }

    static getById(id){
        return  MessageModel.findById(id)
                            .populate('chat')
                            .exec();
    }

    static update(id, mes){
        mes.isEdited = true;
        return  MessageModel.findByIdAndUpdate(id, mes);
    }

    static remove(id){
        return  MessageModel.findByIdAndRemove(id);
    }
};

 module.exports = Message;