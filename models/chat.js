const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const mongoosePaginate = require('mongoose-paginate');

const UserInChatSchema = new Schema({
    user: {type: ObjectId, ref: 'User'},
    role: {type: Number, default: 0},
    lastseenmessage: {type: ObjectId, ref: 'Message'}
});

const ChatSchema = new Schema({
    tag: {type: String, required: true, unique: true},
    name: {type: String, required: true},
    createdAt: {type: Date, default: Date.now()},
    avaUrl: {type: String, default: "/images/default.png"},
    isDisabled: {type: Boolean, default: false },
    isPersonal: {type: Boolean, default: false},
    isPublic: {type: Boolean, default: false},
    isChannel: {type: Boolean, default: false},
    bio: {type: String, required: false},
    usersRoling: [UserInChatSchema],
    users: [{type: ObjectId, ref: 'User'}],
    messages: [{type: ObjectId, ref: 'Message'}],
    lastmessage: {type: ObjectId, ref: 'Message'},
    lastmessagedata: {type: String},
    lastmessagedate: {type: Date}
});

ChatSchema.plugin(mongoosePaginate);

const ChatModel = mongoose.model('Chat', ChatSchema);

class Chat{

    static insert(chat){
        return ChatModel.create(chat);
    }

    static getAll(search){
        const obj = {};
        if (search){
            obj.name = new RegExp(search, "gi");
            obj.isPublic = true;
        } 
        return ChatModel.find(obj);
    }

    static getAll(search, page){
        const obj = {
            name: new RegExp(search, "gi"),
            isPublic: true
        };
        return ChatModel.paginate(obj, { page: page, limit: 20});
    }

    static getById(id){
        return ChatModel.findById(id)
                        .populate('users')
                        .populate('messages')
                        .exec();
    }

    static getByIdAPI(id){
        return ChatModel.findById(id)
                        .populate('messages')
                        .exec();
    }

    static getByTag(tag){
        return ChatModel.findOne({tag: tag});
    }

    static addMessage(message, chatId){
        return this.getById(chatId)
                    .then(data => {
                        data.messages.push(message._id);
                        data.lastmessage = message._id;
                        data.lastmessagedata = message.data;
                        data.lastmessagedate = message.time;
                        return this.update(chatId, data);
                    });
    }

    static addUser(userId, chatId){
        return this.getById(chatId)
                    .then(data => {
                        data.users.push(userId);
                        data.usersRoling.push({user: userId, role: 0});
                        return this.update(chatId, data);
                    });
    }

    static update(id, chat){
        return  ChatModel.findByIdAndUpdate(id, chat);
    }

    static remove(id){
        return  ChatModel.findByIdAndRemove(id);
    }
};

module.exports = Chat;