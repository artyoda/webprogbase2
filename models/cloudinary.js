const config = require('../config');

const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

function cloudinaryUpload(data, callback){
    if (!data)
        callback(undefined);
    else
        cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
                    (error, result) => {
                        callback(result.url);
        })
        .end(data);
}

module.exports = cloudinaryUpload;