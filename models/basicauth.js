const auth = require('basic-auth');

const crypto = require('crypto');
const config = require('../config');
const serverSalt = config.salt;

const User = require('./user');

function sha512(password, salt){
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
}

function autherization(req, res, next){
    const user = auth(req);
    User.getByLoginAndHashPass(user.name, sha512(user.pass, serverSalt).passwordHash)
        .then( foundUser => {
            if (foundUser) next();
            else res.status(401).end('Not authorized');
        });
}

function administration(req, res, next){
    const user = auth(req);
    User.getByLoginAndHashPass(user.name, sha512(user.pass, serverSalt).passwordHash)
        .then( foundUser => {
            if (!foundUser) res.sendStatus(401);
            else if (foundUser.role === 0) res.sendStatus(403);
            else next(); 
        });
}

module.exports = {
    administration: administration,
    autherization: autherization
}