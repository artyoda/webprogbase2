const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const mongoosePaginate = require('mongoose-paginate');

const UserSchema = new Schema({
    login: {type: String, required: true, unique: true},
    fullname: {type: String, required: true},
    password: {type: String, required: true},
    role: {type: Number, default: 0},
    registeredAt: {type: Date, default: Date.now()},
    avaUrl: {type: String, default: "/images/default.png"},
    isDisabled: {type: Boolean, default: false },
    bio: {type: String, default: ''},
    chats: [{type: ObjectId, ref: 'Chat'}]
});

UserSchema.plugin(mongoosePaginate);

const UserModel = mongoose.model('User', UserSchema);

class User{

    static joinChat(userId, chatId){
        return UserModel.findById(userId)
                        .then((user) => {
                            user.chats.push(chatId);
                            return this.update(user._id, user);
                        });
    }

    static insert(user){
        return UserModel.create(user);
    }

    static getAll(page){
        return UserModel.paginate({}, { page: page ? page: 1, limit: 4});
    }

    static getById(id){
        return UserModel.findById(id)
                        .populate('chats')
                        .exec();
    }

    static verificationById(id) {
        return UserModel.findById(id)
            .exec();
    }

    static getByLogin(login){
        return UserModel.findOne({login: login});
    }

    static update(id, user){
        return  UserModel.findByIdAndUpdate(id, user);
    }

    static remove(id){
        return  UserModel.findByIdAndRemove(id);
    }

    static getByLoginAndHashPass(login, password){
        return UserModel.findOne({login: login, password: password});
    }

    static giveRights(id){
        return UserModel.findById(id)
                        .then((user) => {
                            user.role = 1;
                            return user.save();  
                        });                 
    }

    static removeRights(id){
        return UserModel.findById(id)
                        .then((user) => {
                            user.role = 0;
                            return user.save();  
                        });                 
    }
};

 module.exports = User;