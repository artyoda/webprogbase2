const express = require("express");

const Chat = require("../models/chat");
const User = require("../models/user");

const router = express.Router();

const auth = require('../models/basicauth');
const authorize = require('basic-auth');


router.get('/api/v1/chats',
    auth.autherization,
    (req, res, next) => {
        if (!req.query.search)
            User.getByLogin(authorize(req).name)
                .populate('chats')
                .then(user => res.send(user.chats));
        else next();
    },
    async (req, res) => {
        const chats = await Chat.getAll(req.query.search, req.query.page ? parseInt(req.query.page) : 1);
        res.send({
            chats: chats.docs,
            page: chats.page,
            pages: chats.pages,
            search: req.query.search
        });
});

router.get('/api/v1/chat/:id',
    auth.autherization,
    async (req, res) => {
        const chat = await Chat.getById(req.params.id);
        const user = await User.getByLogin(authorize(req).name);
        let cond = false;
        for (let i of chat.users)
            if (i.equals(user._id))
                cond = true;
        if (cond || chat.isPublic)
            res.send(chat);
        else
            res.sendStatus(403);
});

router.post('/api/v1/chat', 
    auth.autherization,
    async (req, res) => {
        let user = await User.getByLogin(authorize(req).name);
        let chat = req.body;
        chat.users = [user._id];
        chat.usersRoling = [{user: user._id, role: 2}];
        chat = await Chat.insert(chat);
        user.chats.push(chat._id);
        await User.update(user._id, user);
        res.status(201).json({id: chat._id});
});

module.exports = router;