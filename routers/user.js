const express = require("express");
const User = require("../models/user");
const cloudinaryUpload = require('../models/cloudinary');

const router = express.Router();
const auth = require('../models/basicauth');

function authenticated(req, res, next) {
    if (req.user) next();
    else res.status(401).end('Not authorized');
}

function checkAdmin(req, res, next) {
    if (!req.user) res.sendStatus(401); 
    else if (req.user.role === 0) res.sendStatus(403); 
    else next();  
}

const authorize = require('basic-auth');

const crypto = require('crypto');
const config = require('../config');
const serverSalt = config.salt;

function sha512(password, salt){
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
}

router.get('/api/v1/user/:id', (req, res) => {
    User.getById(req.params.id)
        .then(user => {
            if (user)
                res.send({
                    login: user.login,
                    fullname: user.fullname,
                    role: user.role === 0 ? 'user' : 'admin',
                    bio: user.bio
                });
            else
                res.sendStatus(404);
    });
});

router.delete('/api/v1/user/:id',
    auth.administration,
    (req, res) => {
        User.remove(req.params.id)
            .then(() => res.status(200).send('deleted'));
});

router.post('/api/v1/user', (req, res) => {
    let data = req.body;
    data.password = sha512(data.password, serverSalt).passwordHash;
    User.getByLogin(data.login)
        .then((user) => {
            if (user)
                res.sendStatus(405);
            else
                User.insert(data)
                    .then((result) => res.status(201).json({id: result._id}));
        });
});

router.put('/api/v1/user',
    auth.autherization,
    (req, res) => {
        const data = req.body; 
        const user = authorize(req);
        User.getByLogin(user.name)
            .then(user => {
                user.fullname = data.fullname ? data.fullname : user.fullname;
                user.bio = data.bio ? data.bio : user.bio;
                User.update(user._id, user)
                    .then(() => res.sendStatus(200));
            });
});

module.exports = router;