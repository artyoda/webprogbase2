const express = require("express");
const path = require('path');

const Message = require('../models/message');
const Chat = require('../models/chat');
const fs = require('fs-extra');
const User = require('../models/user');

const auth = require('../models/basicauth');
const authenticate = require('basic-auth');

const router = express.Router();

router.get('/api/v1/message/:id',
    auth.autherization,
    async (req, res) => {
        const user = await User.getByLogin(authenticate(req).name)
        const message = await Message.getById(req.params.id);
        let cond = false;
        for (let i of message.chat.users)
            if (user._id.equals(i))
                cond = true; 
        if (cond)
            res.send({
                data: message.data,
                sender: message.sender,
                chat: message.chat._id,
                time: message.time
            });
        else
            res.sendStatus(403);
});

router.post('/api/v1/message',
    auth.autherization,
    async (req, res) => {
        let data = req.body;
        const user = await User.getByLogin(authenticate(req).name);
        data.sender = user._id;
        const chat = await Chat.getById(data.chat);
        let cond = false;
        for (let i of chat.users)
            if (user._id.equals(i))
                cond = true;
        if (cond){
            const message = await Message.insert(data);
            await Chat.addMessage(message._id, message.chat);
            res.status(201).json({id: message._id});
        }
        else 
            res.sendStatus(403);        
});

router.put('/api/v1/message/:id',
    auth.autherization,
     async (req, res) => {
        const user = await User.getByLogin(authenticate(req).name);
        let message = await Message.getById(req.params._id);
        if (message.sender.equals(user._id)){
            message.data = req.body.data;
            await Message.update(message._id, message);
            res.sendStatus(200);
        }
        else
            res.sendStatus(403);
                        
});

router.delete('/api/v1/message/:id',
    auth.autherization,
     async (req, res) => {
        const message = await Message.getById(req.params._id);
        const user = await User.getByLogin(authenticate(req).name)
        if(mes.sender.equals(user._id)) {
            await Message.remove(req.params._id);
            res.status(200).send('deleted');
        }
        else
            res.sendStatus(403);           
});

module.exports = router;