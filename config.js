module.exports = {
    dataBaseURL: process.env["DATABASE_URL"] || 'mongodb://localhost:27017/iridium',
    serverPort: process.env["PORT"] || 3000,
    salt: process.env["SALT"] || 'SaLt',
    secret: process.env["SECRET"] || 'SeCrEt',
    jwtSecret: process.env["JWT_SECRET"] || 'SeCrEt',
    cloudinary: {
        cloud_name: process.env["cloudinary_cloud_name"] || 'dxqxitvkd',
        api_key:  process.env["cloudinary_api_key"] || 572822265829497 ,
        api_secret: process.env["cloudinary_api_secret"] || 'hPpz9NTkziOAFM4k0J99rC4naS8'
    }
};